const csv = require("csv-parser"); //importing csv-parser
const fs = require("fs"); //importing file system module

const results = []; //created empty array
fs.createReadStream("../data/matches.csv") //createReadStream method is used to pass data
  .pipe(csv()) //through pipe data will be transfered
  .on("data", (data) => results.push(data)) //adding operation in particular event
  .on("error", (error) => {
    console.log(error);
  })
  .on("end", () => {
    matchesPerYear(results); //calling function
  });

function matchesPerYear(arrayOfMatches) {
  //created function
  const seasons = {}; //created object
  for (let year of arrayOfMatches) {
    if (seasons.hasOwnProperty(year.season)) {
      //if object has particular year
      seasons[year.season]++; //incrementing count
    } else {
      seasons[year.season] = 1; //else created year and added 1
    }
  }
  fs.writeFile(
    //writeFile method is used to write data to a file
    "../public/output/matchesPerYear.json",
    JSON.stringify(seasons), //converting object to json type
    (error) => {
      if (error) {
        console.log(error); //if error occurs it will print it
      }
    }
  );
}
